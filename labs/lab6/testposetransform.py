#!/usr/bin/env python3

'''
This is starter code for Lab 6 on Coordinate Frame transforms.
'''

import asyncio
import cozmo
import numpy
import time
import math
from cozmo.util import degrees

def inverse(rot):
	d = rot.q0*rot.q0 + rot.q1*rot.q1 + rot.q2*rot.q2 + rot.q3*rot.q3
	return cozmo.util.rotation_quaternion(rot.q0/d, -rot.q1/d, -rot.q2/d, -rot.q3/d)

def makeTraMat(pos):
	return numpy.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [pos.x, pos.y, pos.z, 1]])

def makeRotMat(angle, x, y, z):
	cosangle = math.cos(angle)
	sinangle = math.sin(angle)
	minuscosangle = 1.0 - cosangle

	m00 = cosangle + x * x * minuscosangle
	m11 = cosangle + y * y * minuscosangle
	m22 = cosangle + z * z * minuscosangle
	m10 = x * y * minuscosangle + z * sinangle
	m01 = x * y * minuscosangle - z * sinangle
	m20 = x * z * minuscosangle + y * sinangle
	m02 = x * z * minuscosangle - y * sinangle
	m21 = y * z * minuscosangle + x * sinangle
	m12 = y * z * minuscosangle - x * sinangle

	return numpy.array([[m00, m01, m02, 0], [m10, m11, m12, 0], [m20, m21, m22, 0], [0, 0, 0, 1]])

def getQuatAxis(rot):
	sqrtw = math.sqrt(1 - rot.q0 * rot.q0)
	if sqrtw > 0.01:
		rotx = rot.q1 / sqrtw
		roty = rot.q2 / sqrtw
		rotz = rot.q3 / sqrtw
	else:
		rotx = 0
		roty = 0
		rotz = 1
	return numpy.array([rotx, roty, rotz])

def makeMat(rot,pos):
	rotangle = 2*math.acos(rot.q0)
	rotaxis = getQuatAxis(rot)
	rotatemat = makeRotMat(rotangle, rotaxis[0], rotaxis[1], rotaxis[2])
	translatemat = makeTraMat(pos)
	return numpy.matmul(rotatemat, translatemat)

def makeQuat(m):
	angle = math.acos((m[0][0] + m[1][1] + m[2][2] - 1)*0.5)
	onetwosquare = (m[2][1] - m[1][2]) ** 2
	zerotwosquare = (m[0][2] - m[2][0]) ** 2
	zeroonesquare = (m[1][0] - m[0][1]) ** 2
	denom = math.sqrt(onetwosquare + zerotwosquare + zeroonesquare)
	x = (m[2][1] - m[1][2]) / denom
	y = (m[0][2] - m[2][0]) / denom
	z = (m[1][0] - m[0][1]) / denom

	halfangle = angle*0.5
	qw = math.cos(halfangle)
	qx = x * math.sin(halfangle)
	qy = y * math.sin(halfangle)
	qz = z * math.sin(halfangle)
	return cozmo.util.rotation_quaternion(qw, qx, qy, qz)



def get_relative_pose(object_pose, reference_frame_pose):
	refmat = makeMat(reference_frame_pose.rotation, reference_frame_pose.position)
	objmat = makeMat(object_pose.rotation, object_pose.position)
	refbasis = numpy.linalg.inv(refmat)

	modified = numpy.matmul(refbasis,objmat)
	modifiedQuat = makeQuat(modified)

	return cozmo.util.pose_quaternion(modified[3][0], modified[3][1], modified[3][2], modifiedQuat.q0, modifiedQuat.q1, modifiedQuat.q2, modifiedQuat.q3)


#robopose = cozmo.util.pose_quaternion(10, -4, 0.0, 1.0, 0.0, 0.0, 0.0)
#destination = cozmo.util.pose_quaternion(203.3, 7, 15, 0.9242, 0.0, 0.0, 0.3817)
robopose = cozmo.util.pose_quaternion(0,0,0, 1.0, 0.0, 0.0, 0.0)
destination = cozmo.util.pose_quaternion(100, 100, 100, 0.9242, 0.0, 0.0, 0.3817)

rel = get_relative_pose(destination, robopose)
wheel_base = 83.9
rel_x = rel.position.x
rel_y = rel.position.y
rel_angle = 2 * math.acos(rel.rotation.q0)
distance = math.sqrt(rel_y*rel_y + rel_x*rel_x)

left_speed = distance - (rel_angle*wheel_base)/2
right_speed = distance + (rel_angle*wheel_base)/2
avg_speed = left_speed * 0.5 + right_speed * 0.5
time = distance/avg_speed;
