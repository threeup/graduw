#!/usr/bin/env python3

'''
This is starter code for Lab 6 on Coordinate Frame transforms.
'''

import asyncio
import cozmo
import numpy
import time
import math
from cozmo.util import degrees

def make_tra_matrix_from_pos(pos):
	return numpy.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [pos.x, pos.y, pos.z, 1]])

def make_rot_matrix_from_angle_axis(angle, x, y, z):
	cosangle = math.cos(angle)
	sinangle = math.sin(angle)
	minuscosangle = 1.0 - cosangle

	m00 = cosangle + x * x * minuscosangle
	m11 = cosangle + y * y * minuscosangle
	m22 = cosangle + z * z * minuscosangle
	m10 = x * y * minuscosangle + z * sinangle
	m01 = x * y * minuscosangle - z * sinangle
	m20 = x * z * minuscosangle + y * sinangle
	m02 = x * z * minuscosangle - y * sinangle
	m21 = y * z * minuscosangle + x * sinangle
	m12 = y * z * minuscosangle - x * sinangle

	return numpy.array([[m00, m01, m02, 0], [m10, m11, m12, 0], [m20, m21, m22, 0], [0, 0, 0, 1]])

def make_axis_from_quat(rot):
	sqrtw = math.sqrt(1 - rot.q0 * rot.q0)
	if sqrtw > 0.01:
		rotx = rot.q1 / sqrtw
		roty = rot.q2 / sqrtw
		rotz = rot.q3 / sqrtw
	else:
		rotx = 0
		roty = 0
		rotz = 1
	return numpy.array([rotx, roty, rotz])

def make_matrix_rot_pos(rot, pos):
	rotangle = 2*math.acos(rot.q0)
	rotaxis = make_axis_from_quat(rot)
	rotatemat = make_rot_matrix_from_angle_axis(rotangle, rotaxis[0], rotaxis[1], rotaxis[2])
	translatemat = make_tra_matrix_from_pos(pos)
	return numpy.matmul(rotatemat, translatemat)

def make_quat_matrix(m):
	angle = math.acos((m[0][0] + m[1][1] + m[2][2] - 1)*0.5)
	onetwosquare = (m[2][1] - m[1][2]) ** 2
	zerotwosquare = (m[0][2] - m[2][0]) ** 2
	zeroonesquare = (m[1][0] - m[0][1]) ** 2
	denom = math.sqrt(onetwosquare + zerotwosquare + zeroonesquare)
	x = (m[2][1] - m[1][2]) / denom
	y = (m[0][2] - m[2][0]) / denom
	z = (m[1][0] - m[0][1]) / denom

	halfangle = angle*0.5
	qw = math.cos(halfangle)
	qx = x * math.sin(halfangle)
	qy = y * math.sin(halfangle)
	qz = z * math.sin(halfangle)
	return cozmo.util.rotation_quaternion(qw, qx, qy, qz)

def make_quat_axis(angle, x, y, z):
	half_radians = angle * 0.5 * math.pi / 180
	qw = math.cos(half_radians)
	qx = 0
	qy = 0
	qz = math.sin(half_radians)
	return cozmo.util.rotation_quaternion(qw, qx, qy, qz)


def get_relative_pose(object_pose, reference_frame_pose):
	refmat = make_matrix_rot_pos(reference_frame_pose.rotation, reference_frame_pose.position)
	objmat = make_matrix_rot_pos(object_pose.rotation, object_pose.position)
	refbasis = numpy.linalg.inv(refmat)

	modified = numpy.matmul(refbasis, objmat)
	modifiedQuat = make_quat_matrix(modified)

	return cozmo.util.pose_quaternion(modified[3][0], modified[3][1], modified[3][2], modifiedQuat.q0, modifiedQuat.q1, modifiedQuat.q2, modifiedQuat.q3)


def find_relative_cube_pose(robot: cozmo.robot.Robot):
	'''Looks for a cube while sitting still, prints the pose of the detected cube
	in world coordinate frame and relative to the robot coordinate frame.'''

	robot.move_lift(-3)
	robot.set_head_angle(degrees(0)).wait_for_completed()



	while True:
		time.sleep(.5)
		try:
			cube = robot.world.wait_for_observed_light_cube(timeout=30)
			if cube:
				print("Robot pose: %s" % robot.pose)
				print("Cube pose: %s" % cube.pose)
				print("Cube pose in the robot coordinate frame: %s" % get_relative_pose(cube.pose, robot.pose))
		except asyncio.TimeoutError:
			print("Didn't find a cube")


if __name__ == '__main__':

	cozmo.run_program(find_relative_cube_pose)
