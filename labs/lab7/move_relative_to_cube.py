#!/usr/bin/env python3

'''
This is starter code for Lab 7.

'''

import cozmo
from cozmo.util import degrees, Angle, Pose, distance_mm, speed_mmps
import math
import time
import sys
import asyncio

from odometry import my_go_to_pose3
sys.path.insert(0, '../lab6')
from pose_transform import get_relative_pose




def move_relative_to_cube(robot: cozmo.robot.Robot):
	'''Looks for a cube while sitting still, when a cube is detected it 
	moves the robot to a given pose relative to the detected cube pose.'''

	robot.move_lift(-3)
	robot.set_head_angle(degrees(0)).wait_for_completed()
	cube = None
	print("Robot battery " + str(robot.battery_voltage))
	print("Robot pose: %s" % robot.pose)

	while cube is None:
		try:
			cube = robot.world.wait_for_observed_light_cube(timeout=30)
			if cube:
				print("Found a cube, pose in the robot coordinate frame: %s" % get_relative_pose(cube.pose, robot.pose))
		except asyncio.TimeoutError:
			print("Didn't find a cube")

	desired_pose_relative_to_cube = Pose(0, 100, 0, angle_z=degrees(90))
	#getting relative from the cube is weird because the cube up vector is odd

	pose_relative = get_relative_pose(desired_pose_relative_to_cube, robot.pose)

	#combine the destination tip to tail
	destination_x = cube.pose.position.x - pose_relative.position.x
	destination_y = cube.pose.position.y - pose_relative.position.y

	rel_x = destination_x - robot.pose.position.x
	rel_y = destination_y - robot.pose.position.y
	
	# the final orientation should be the original relative angle difference
	rel_deg = math.atan2(-rel_y, rel_x)*180/math.pi

	my_go_to_pose3(robot, destination_x, destination_y, rel_deg)

	print("Robot pose: %s" % robot.pose)


if __name__ == '__main__':

	cozmo.run_program(move_relative_to_cube)
