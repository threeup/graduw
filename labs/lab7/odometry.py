#!/usr/bin/env python3

'''
Stater code for Lab 7.

'''

import cozmo
from cozmo.util import degrees, Angle, Pose, distance_mm, speed_mmps
import math
import time
import sys
sys.path.insert(0, '../lab6')
from pose_transform import get_relative_pose, make_quat_axis

# Wrappers for existing Cozmo navigation functions

def cozmo_drive_straight(robot, dist, speed):
	"""Drives the robot straight.
		Arguments:
		robot -- the Cozmo robot instance passed to the function
		dist -- Desired distance of the movement in millimeters
		speed -- Desired speed of the movement in millimeters per second
	"""
	robot.drive_straight(distance_mm(dist), speed_mmps(speed)).wait_for_completed()

def cozmo_turn_in_place(robot, angle, speed):
	"""Rotates the robot in place.
		Arguments:
		robot -- the Cozmo robot instance passed to the function
		angle -- Desired distance of the movement in degrees
		speed -- Desired speed of the movement in degrees per second
	"""
	robot.turn_in_place(degrees(angle), speed=degrees(speed)).wait_for_completed()

def cozmo_go_to_pose(robot, x, y, angle_z):
	"""Moves the robot to a pose relative to its current pose.
		Arguments:
		robot -- the Cozmo robot instance passed to the function
		x,y -- Desired position of the robot in millimeters
		angle_z -- Desired rotation of the robot around the vertical axis in degrees
	"""
	robot.go_to_pose(Pose(x, y, 0, angle_z=degrees(angle_z)), relative_to_robot=True).wait_for_completed()

# Functions to be defined as part of the labs

def get_front_wheel_radius():
	"""Returns the radius of the Cozmo robot's front wheel in millimeters."""
	return 13.53
	# repeated tests to move x amount for a single revolution, where x vary between 50 and 100.  
	# 85 was the closest distance for a single revolution 
	# c = 2*pi*r  , r = c / 2*pi, = 13.53

def get_distance_between_wheels():
	"""Returns the distance between the wheels of the Cozmo robot in millimeters."""
	return 41.9
	# a fixed speed for a wheel was chosen, 29 mm/s, the negative is applied to the opposite wheel
	# A complete rotation was accomplished by driving for 10 seconds
	# The equivalent straight line path, with same duration and positive fixed speed in each wheel was 3.1 wheel revolutions
	# at 85 mm per revolution = each wheel travels 263.55mm
	# In the rotation experiment, the outer wheel also travels 263.55mm which makes the radius of that circle 263.55/2*pi = 41.9

def rotate_front_wheel(robot, angle_deg):
	"""Rotates the front wheel of the robot by a desired angle.
		Arguments:
		robot -- the Cozmo robot instance passed to the function
		angle_deg -- Desired rotation of the wheel in degrees
	"""
	speed = 10
	dist = get_front_wheel_radius()*angle_deg/360
	robot.drive_straight(distance_mm(dist), speed_mmps(speed)).wait_for_completed()

def my_drive_straight(robot, dist, speed):
	"""Drives the robot straight.
		Arguments:
		robot -- the Cozmo robot instance passed to the function
		dist -- Desired distance of the movement in millimeters
		speed -- Desired speed of the movement in millimeters per second
	"""
	time = dist / speed
	robot.drive_wheels(speed, speed, 9999, 9999, time+0.1)

def my_turn_in_place(robot, angle, speed):
	"""Rotates the robot in place.
		Arguments:
		robot -- the Cozmo robot instance passed to the function
		angle -- Desired distance of the movement in degrees
		speed -- Desired speed of the movement in degrees per second
	"""
	time = math.fabs(angle) / speed
	circ = get_distance_between_wheels() * 2 * math.pi
	fudge = 1.1
	dist = fudge * circ * angle / 360
	wheelspeed = dist / time
	robot.drive_wheels(wheelspeed, -wheelspeed, 9999, 9999, time)
	pass

def my_go_to_pose1(robot, x, y, angle_z):
	"""Moves the robot to a pose relative to its current pose.
		Arguments:
		robot -- the Cozmo robot instance passed to the function
		x,y -- Desired position of the robot in millimeters
		angle_z -- Desired rotation of the robot around the vertical axis in degrees
	"""

	quat = make_quat_axis(angle_z, 0, 0, 1)
	destination = cozmo.util.pose_quaternion(x, y, 0, quat.q0, quat.q1, quat.q2, quat.q3)
	print("destination pose: %s" % destination)
	rel = get_relative_pose(destination, robot.pose)
	rel_x = rel.position.x
	rel_y = rel.position.y
	rel_degrees = 2*math.acos(rel.rotation.q0) * 180/math.pi

	angle_toward_destination = math.atan2(rel_y,rel_x)*180/math.pi
	my_turn_in_place(robot, angle_toward_destination, 20)
	distance = math.sqrt(rel_y*rel_y + rel_x*rel_x)
	time.sleep(0.1)
	my_drive_straight(robot, distance, 20)
	angle_toward_pose = rel_degrees - angle_toward_destination
	time.sleep(0.1)
	my_turn_in_place(robot, angle_toward_pose, 20)
	pass

def my_go_to_pose2(robot, x, y, angle_z):
	"""Moves the robot to a pose relative to its current pose.
		Arguments:
		robot -- the Cozmo robot instance passed to the function
		x,y -- Desired position of the robot in millimeters
		angle_z -- Desired rotation of the robot around the vertical axis in degrees
	""" 
	wheel_base = get_distance_between_wheels()

	rel_x = robot.pose.position.x - x
	rel_y = robot.pose.position.y - y
	desired_radians = angle_z * math.pi / 180
	expected_distance = math.sqrt(rel_y*rel_y + rel_x*rel_x)
	attempts = (1.6 * expected_distance) / 50
	attempts = max(1, int(attempts))
	last_distance = 2*expected_distance
	for attempt in range(0, attempts):

		robot_facing = 2*math.acos(robot.pose.rotation.q0)
		rel_x = x - robot.pose.position.x
		rel_y = y - robot.pose.position.y
		move_radians = math.atan2(-rel_y, rel_x)
		distance = math.sqrt(rel_y*rel_y + rel_x*rel_x)
		
		# fudge numbers can tune some inaccuracies due to physical friction or motors, etc
		fudge_one = 1.0
		fudge_two = 1.0
		fudge_three = 1.0

		x_dot = fudge_one*50
		alpha = robot_facing - move_radians
		if alpha > math.pi:
			alpha -= 2*math.pi
		elif alpha < -math.pi:
			alpha += 2*math.pi

		eta = desired_radians - robot_facing
		theta_dot = fudge_two * alpha + fudge_three * eta
		print("x_dot: %s theta_dot: %s" % (x_dot, theta_dot))
		print("distance: %s %s %s" % (distance, rel_x, rel_y))
		left_speed = 2 * x_dot - theta_dot * wheel_base
		right_speed = 2 * x_dot + theta_dot * wheel_base
		total_speed = left_speed * 0.5 + right_speed * 0.5

		# dont go at full speed, adjust so that the average speed is 30
		avg_speed = 30
		left_speed *= avg_speed/total_speed
		right_speed *= avg_speed/total_speed
		last_step = False
		step = 50
		if distance < 20:
			# if we are close to the destination, clamp step to the remainder
			step = distance
			last_step = True
		if last_distance < distance:
			# if we are driving further away from our destination, just stop
			step = 5
			last_step = True

		last_distance = distance
		drive_time = step/avg_speed
		robot.drive_wheels(left_speed, right_speed, 9999, 9999, drive_time)
		time.sleep(0.33)
		if last_step:
			break

	# a final rotation to fix it up
	robot_facing = 2*math.acos(robot.pose.rotation.q0)*180/math.pi
	rotate_degrees = (angle_z - robot_facing)
	my_turn_in_place(robot, rotate_degrees, 30)
	pass

def my_go_to_pose3(robot, x, y, angle_z):
	"""Moves the robot to a pose relative to its current pose.
		Arguments:
		robot -- the Cozmo robot instance passed to the function
		x,y -- Desired position of the robot in millimeters
		angle_z -- Desired rotation of the robot around the vertical axis in degrees
	"""
	quat = make_quat_axis(angle_z, 0, 0, 1)
	destination = cozmo.util.pose_quaternion(x, y, 0, quat.q0, quat.q1, quat.q2, quat.q3)
	rel = get_relative_pose(destination, robot.pose)
	rel_x = rel.position.x
	rel_y = rel.position.y
	first_degrees = math.atan2(rel_y, rel_x)

	#an initial rotation to align to something out of view, anything beyond 90degrees
	too_far_angle = math.pi / 2
	if first_degrees < -too_far_angle or first_degrees > too_far_angle:
		instant_angle = (too_far_angle - first_degrees)*180/math.pi
		my_turn_in_place(robot, instant_angle, 20)

	my_go_to_pose2(robot, x, y, angle_z)
	pass

def run(robot: cozmo.robot.Robot):

	print("***** Front wheel radius: " + str(get_front_wheel_radius()))
	print("***** Distance between wheels: " + str(get_distance_between_wheels()))
	print("Robot battery " + str(robot.battery_voltage))
	print("Robot pose: %s" % robot.pose)
	## Example tests of the functions

	#rotate_front_wheel(robot, 360)

	#cozmo_drive_straight(robot, 62, 50)
	#cozmo_turn_in_place(robot, 60, 30)
	#cozmo_go_to_pose(robot, 100, 100, 45)

	#my_drive_straight(robot, 62, 50)

	#my_drive_straight(robot, 200, 50)
	my_turn_in_place(robot, -55, 30)

	#my_go_to_pose1(robot, 100, 100, 45)
	#my_go_to_pose2(robot, 180, -50, 0)
	#my_go_to_pose3(robot, -100, 100, 45)
	print("Final pose: %s" % robot.pose)


if __name__ == '__main__':

	cozmo.run_program(run)



