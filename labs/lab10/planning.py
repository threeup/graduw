
#author1:
#author2:

from grid import *
from visualizer import *
import threading

from queue import PriorityQueue
import math
import cozmo

from cozmo.util import distance_mm, speed_mmps, degrees, radians
sys.path.insert(0, '../lab6')


def astar(grid, heuristic):
	"""Perform the A* search algorithm on a defined grid

		Arguments:
		grid -- CozGrid instance to perform search on
		heuristic -- supplied heuristic function
	"""
	open_set = []
	links = {}

	goals = grid.getGoals()
	if len(goals) == 0:
		return
	grid.clearVisited()
	start_coord = grid.getStart()
	current_coord = start_coord
	current_idx = current_coord[0]*100+current_coord[1]
	open_set.append(current_coord)
	links[current_idx] = (None, 0, heuristic(current_coord, goals[0]))
	found_goal = False
	while len(open_set) > 0 :
		current_coord = open_set.pop(0)
		current_idx = current_coord[0]*100+current_coord[1]
		current_cost = links[current_idx][1]
		grid.addVisited(current_coord)
		if current_coord in goals:
			grid.setPath(buildpath(current_coord, links))
			found_goal = True
			break

		nbors = grid.getNeighbors(current_coord)
		for nbor in nbors:
			nbor_coord = nbor[0]
			nbor_idx = nbor_coord[0] * 100 + nbor_coord[1]
			step_cost = 1
			if nbor_coord[0] != current_coord[0] and nbor_coord[1] != current_coord[1]:
				step_cost = 1.414
			next_cost = current_cost + step_cost
			next_heuristic = heuristic(nbor_coord, goals[0])
			next_sum = next_cost + next_heuristic
			visited = grid.getVisited()
			if nbor_coord in visited and nbor_coord not in goals:
				continue

			nbor_link = links.get(nbor_idx)
			if nbor_link is not None:
				if nbor_link[1] > next_cost:
					links[nbor_idx] = (current_coord, next_cost, next_sum)
			else:
				links[nbor_idx] = (current_coord, next_cost, next_sum)
				inserted = False
				for i in range(len(open_set)):
					node_coord = open_set[i]
					node_idx = node_coord[0] * 100 + node_coord[1]
					if links[node_idx][2] > next_sum:
						open_set.insert(i, nbor_coord)
						inserted = True
						break
				if inserted is False:
					open_set.append(nbor_coord)

	if found_goal is False:
		path = [start_coord]
		grid.setPath(path)


def buildpath(node, links):
	"""returns a path given a set of links and an end point
			"""
	path = []
	current = node
	while current is not None:
		path.append(current)
		current_idx = current[0]*100+current[1]
		current = links[current_idx][0]

	path.reverse()
	return path


def heuristic(current, goal):
	"""Heuristic function for A* algorithm

		Arguments:
		current -- current cell
		goal -- desired goal cell
	"""
	manhattan_x = goal[0] - current[0]
	manhattan_y = goal[1] - current[1]
	return math.sqrt(manhattan_x*manhattan_x + manhattan_y*manhattan_y)
	

def rotate(robot, center, dest_angle):
	"""rotates the robot to an angle in degrees, requires reference point as center
	"""
	center_h = center[2]
	robot_h = robot.pose.rotation.angle_z.degrees - center_h
	delta_angle = dest_angle - robot_h

	if delta_angle > 180:
		delta_angle -= 360
	if delta_angle < -180:
		delta_angle += 360

	if delta_angle > 30:
		delta_angle = 30

	if delta_angle < -30:
		delta_angle = -30

	rotate_speed = radians(0.9)
	rotate_accel = radians(1.0)
	robot.turn_in_place(degrees(delta_angle), False, 0, rotate_speed, rotate_accel).wait_for_completed()
	return abs(delta_angle) > 29


def drive(robot, x, y, center, finalize):
	"""Moves the robot to a coordinate, requires reference point as center
	"""

	raw_before = get_raw_pos(center, robot.pose)
	raw_after = raw_before
	delta_x = x - raw_before[0]
	delta_y = y - raw_before[1]

	delta_sum = abs(delta_x)+abs(delta_y)

	dest_angle = math.atan2(delta_y, delta_x) * 180 / math.pi

	if delta_sum > 0.5:
		if rotate(robot, center, dest_angle):
			return False

	delta_distance = get_real_distance(delta_x, delta_y)
	straight_speed = 40
	original_position = robot.pose.position
	if finalize:
		delta_distance -= 50

	last_remaining_x = 999
	last_remaining_y = 999
	for i in range(4):
		step_distance = 0.25*delta_distance

		robot.drive_straight(distance_mm(step_distance), speed_mmps(straight_speed), False).wait_for_completed()
		raw_after = get_raw_pos(center, robot.pose)
		remaining_x = abs(raw_after[0] - x)
		remaining_y = abs(raw_after[1] - y)
		#print("i %s desired: %s %s now %s %s" % (i, x, y, raw_after[0], raw_after[1]))
		if remaining_x < 0.2 and remaining_y < 0.2:
			break
		if remaining_x > last_remaining_x:
			break
		if remaining_y > last_remaining_y:
			break
		last_remaining_x = remaining_x
		last_remaining_y = remaining_y

	position_change_x = robot.pose.position.x - original_position.x
	position_change_y = robot.pose.position.y - original_position.y
	distance = math.sqrt(position_change_x*position_change_x + position_change_y*position_change_y)
	if distance > 4*delta_distance and distance > 150:
		# anomaly detected, invalidate everything
		return True

	moved_x = raw_after[0] - raw_before[0]
	moved_y = raw_after[1] - raw_before[1]
	moved_sum = abs(moved_x) + abs(moved_y)
	if moved_sum > 4*delta_sum and delta_sum > 0.4:
		# anomaly detected, invalidate everything
		return True

	#print("desired %s %s moved %s %s" % (delta_x, delta_y, moved_x, moved_y))
	return False


def get_real_distance(delta_x, delta_y):
	"""returns the real distance given a grid coordinate x and y
		"""
	grid_size = 25
	return math.sqrt(delta_x * delta_x + delta_y * delta_y) * grid_size


def grid_to_real(center, grid_x, grid_y):
	"""returns a real coordinate in robot coordinates from a given grid x and y, requires reference center
		"""
	center_h = center[2]
	r = center_h*math.pi/180
	grid_size = 25
	real_x = center[0] + (grid_x*math.cos(r) - grid_y*math.sin(r))*grid_size
	real_y = center[1] + (grid_x*math.sin(r) + grid_y*math.cos(r))*grid_size
	return real_x, real_y


def get_raw_pos(center, object_pose):
	"""returns a grid position as a float given an objects pose, requires reference center
		"""
	center_h = -center[2]
	r = center_h * math.pi / 180
	grid_size = 25
	object_x = object_pose.position.x - center[0]
	object_y = object_pose.position.y - center[1]
	grid_x = (object_x * math.cos(r) - object_y * math.sin(r))/grid_size
	grid_y = (object_x * math.sin(r) + object_y * math.cos(r))/grid_size
	return grid_x, grid_y


def get_grid_pos(center, object_pose):
	"""returns a grid position as rounded whole number given an objects pose, requires reference center
			"""
	grid_x, grid_y = get_raw_pos(center, object_pose)
	return max(0, round(grid_x)), max(0, round(grid_y))


def get_wide_pos(center, object_pose, buffer):
	"""returns a list of grid positions which includes some wideness given an objects pose, requires reference center
			"""
	grid_x, grid_y = get_raw_pos(center, object_pose)
	min_x = math.floor(grid_x-buffer)
	min_y = math.floor(grid_y-buffer)
	max_x = math.ceil(grid_x+buffer)
	max_y = math.ceil(grid_y+buffer)
	result = []
	for i in range(min_x, max_x):
		for j in range(min_y, max_y):
			result.append((i, j))
	return result


def refresh_locations(origin, robot_pose, goal_pose, ob_one_pose, ob_two_pose):
	"""reevaluate all observed entities
			"""
	dirty = False
	robot_grid = get_grid_pos(origin, robot_pose)
	start = robot_grid
	if grid.getStart() != start:
		grid.setStart(start)
	goal_blob = []
	if goal_pose is not None:
		goal_blob = get_wide_pos(origin, goal_pose, 0.5)
		for goal in goal_blob:
			if goal not in grid.getGoals():
				dirty = True
	ob_one_blob = []
	ob_two_blob = []
	if ob_one_pose is not None:
		ob_one_blob = get_wide_pos(origin, ob_one_pose, 1.9)
		for ob_one in ob_one_blob:
			if ob_one not in grid._obstacles:
				dirty = True
	if ob_two_pose is not None:
		ob_two_blob = get_wide_pos(origin, ob_two_pose, 1.9)
		for ob_two in ob_two_blob:
			if ob_two not in grid._obstacles:
				dirty = True

	if dirty:
		if len(goal_blob) > 0:
			grid.clearGoals()
		grid.clearObstacles()
		for goal in goal_blob:
			grid.addGoal(goal)
		for ob_one in ob_one_blob:
			grid.addObstacle(ob_one)
		for ob_two in ob_two_blob:
			grid.addObstacle(ob_two)
	return dirty

def cozmoBehavior(robot: cozmo.robot.Robot):
	"""Cozmo search behavior. See assignment description for details

		Has global access to grid, a CozGrid instance created by the main thread, and
		stopevent, a threading.Event instance used to signal when the main thread has stopped.
		You can use stopevent.is_set() to check its status or stopevent.wait() to wait for the
		main thread to finish.

		Arguments:
		robot -- cozmo.robot.Robot instance, supplied by cozmo.run_program
	"""

	global grid, stopevent

	goal_cube_pose = None
	ob_one_pose = None
	ob_two_pose = None
	path = []
	robot.move_lift(-3)
	robot.set_head_angle(cozmo.robot.MIN_HEAD_ANGLE * 0.9 + cozmo.robot.MAX_HEAD_ANGLE * 0.1).wait_for_completed()
	satisfied_center = False
	satisfied_goal_cube = False
	found_goal_cube = False
	grid.addGoal((12, 7))

	origin_x = robot.pose.position.x
	origin_y = robot.pose.position.y
	origin_h = robot.pose.rotation.angle_z.radians
	origin = (origin_x, origin_y, origin_h)
	print("Robot battery " + str(robot.battery_voltage))
	while not stopevent.is_set() and not satisfied_goal_cube:
		for obj in robot.world.visible_objects:
			if hasattr(obj, 'cube_id'):
				if obj.cube_id == cozmo.objects.LightCube1Id:
					goal_cube_pose = obj.pose
					found_goal_cube = True;
				if obj.cube_id == cozmo.objects.LightCube2Id:
					ob_one_pose = obj.pose
				if obj.cube_id == cozmo.objects.LightCube3Id:
					ob_two_pose = obj.pose
		if found_goal_cube is False and satisfied_center is True:
			robot.drive_straight(distance_mm(25), speed_mmps(80), False).wait_for_completed()
			robot.turn_in_place(degrees(15), False, 0, radians(0.5), radians(0.5)).wait_for_completed()
			robot_grid = get_grid_pos(origin, robot.pose)
			grid.setStart(robot_grid)
			continue

		satisfied = False
		anomaly = False
		if refresh_locations(origin, robot.pose, goal_cube_pose, ob_one_pose, ob_two_pose):
			path = []
		if len(path) == 0:
			robot_grid = get_grid_pos(origin, robot.pose)
			print("path %s,%s to %s,%s " % (robot_grid[0], robot_grid[1], grid.getGoals()[0][0], grid.getGoals()[0][1]))
			grid.setStart(robot_grid)
			satisfied = grid.getStart() in grid.getGoals()
			if satisfied is False:
				astar(grid, heuristic)
				path = grid.getPath()
				print("generated path len:%s" % (len(path)))
				# pop one from the start
				if len(path) > 0:
					if path[0] == grid.getStart():
						path.pop(0)
				# pop two from the end
				if len(path) > 0 and found_goal_cube is True:
					path.pop()
				if len(path) == 0:
					satisfied = True
		else:
			step = path[0]
			robot_grid = get_grid_pos(origin, robot.pose)
			step_dist = abs(robot_grid[0] - step[0]) + abs(robot_grid[1] - step[1])
			print("step to: %s %s, dist %s path len %s" % (step[0], step[1], step_dist, len(path)))
			if step_dist > 0:
				previous_position = robot.pose.position
				anomaly = drive(robot, step[0], step[1], origin, False)
				robot_grid = get_grid_pos(origin, robot.pose)
				step_dist = abs(robot_grid[0] - step[0]) + abs(robot_grid[1] - step[1])
				print("end at: %s %s, dist %s" % (robot_grid[0], robot_grid[1], step_dist))
				if anomaly:
					print("anomaly: %s %s" % (previous_position, robot.pose))
					goal_cube_pose = None
					ob_one_pose = None
					ob_two_pose = None
					found_goal_cube = False
					path = []
			if step_dist == 0 and len(path) > 0:
				path.pop(0)
			if len(path) == 0:
				satisfied = True
		if satisfied is True and anomaly is False:
			if found_goal_cube is False:
				print("satisfied center")
				robot.turn_in_place(degrees(30), False, 0, radians(0.5), radians(0.5)).wait_for_completed()
				satisfied_center = True
			else:
				print("satisfied goal cube")
				satisfied_goal_cube = True

	goal_x = grid.getGoals()[0][0]
	goal_y = grid.getGoals()[0][1]
	print("last align %s %s" % (goal_x, goal_y))

######################## DO NOT MODIFY CODE BELOW THIS LINE ####################################


class RobotThread(threading.Thread):
	"""Thread to run cozmo code separate from main thread
	"""

	def __init__(self):
		threading.Thread.__init__(self, daemon=True)

	def run(self):
		cozmo.run_program(cozmoBehavior)


# If run as executable, start RobotThread and launch visualizer with empty grid file
if __name__ == "__main__":
	global grid, stopevent
	stopevent = threading.Event()
	grid = CozGrid("emptygrid.json")
	visualizer = Visualizer(grid)
	updater = UpdateThread(visualizer)
	updater.start()
	robot = RobotThread()
	robot.start()
	visualizer.start()
	stopevent.set()

