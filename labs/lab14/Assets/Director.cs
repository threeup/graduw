﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Director : MonoBehaviour {

	public GameObject botSpawn;
	public List<GameObject> obstacleSpawnList;
	public Obstacle oproto;
	public Playerbot pproto;
	public Aibot aproto;
	public Mainbot p;
	public List<Obstacle> o;
	public List<Obstacle> obstacles;
	public Collider lefthazard;
	public Collider righthazard;
	public Collider tophazard;
	public Collider bothazard;
	public int Reward;
	public int LastReward;
	// Use this for initialization
	void Start () {
		botSpawn.SetActive(false);
		foreach(GameObject objspawn in obstacleSpawnList)
		{
			objspawn.SetActive(false);
		}
		Make(true);
	}

	void Make(bool bIsPlayer)
	{
		if(bIsPlayer)
		{
			p = GameObject.Instantiate(pproto, botSpawn.transform.position + Vector3.up*1, Quaternion.identity).GetComponent<Playerbot>();
		}
		else
		{
			p = GameObject.Instantiate(aproto, botSpawn.transform.position + Vector3.up*1, Quaternion.identity).GetComponent<Aibot>();
		}
		p.director = this;
		foreach(GameObject objspawn in obstacleSpawnList)
		{
			obstacles.Add(GameObject.Instantiate(oproto, objspawn.transform.position, Quaternion.identity).GetComponent<Obstacle>());
		}
		o.AddRange(obstacles);
		Reward = 0;
	}
	
	// Update is called once per frame
	void Update () {
		o.RemoveAll(x => x.transform.position.y < -1);
		bool gameover = false; 
		if(p)
		{
			Reward = (obstacleSpawnList.Count - o.Count)*2;
			Debug.Log(Reward);
			if (p.transform.position.y < -1)
			{
				Reward -= 5;
				gameover = true;
			}
			else if (o.Count == 0)
			{
				gameover = true;
			}
		}
		if(gameover)
		{
			LastReward = Reward;
			foreach(Obstacle obs in obstacles)
			{
				Destroy(obs.gameObject);
			}
			obstacles.Clear();
			o.Clear();
			Destroy(p.gameObject);
			Make(p is Aibot);
		}
	}

	public void PlayerRetarget()
	{
		p.Retarget(o);
	}

	public void PlayerForward(bool bIsForward)
	{
		if(bIsForward)
		{
			p.SetTarget(p.transform.position + p.transform.forward * 2.0f);
		}
		else
		{
			p.SetTarget(p.transform.position - p.transform.forward * 2.0f);
		}
	}

	public void PlayerLateral(bool bIsClockwise)
	{
		if(bIsClockwise)
		{
			p.SetTarget(p.transform.position + p.transform.right * 2.0f);
		}
		else
		{
			p.SetTarget(p.transform.position - p.transform.right * 2.0f);
		}
	}

	
}
