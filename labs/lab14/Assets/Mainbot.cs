﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mainbot : MonoBehaviour {

    public Obstacle obstacle;
    public Director director;
    
    public Vector3 targetLocation;

    public float distanceToHazard;
    public float distanceToTarget;
    public float distanceToObstacle;
    public Vector3 diffToTarget;
    public bool nearObstacle;
    public bool nearHazard;
    void Start() {
        
    }
    public void Measure() {
        Vector3 closestLeft = director.lefthazard.ClosestPointOnBounds(transform.position) - transform.position;
        Vector3 closestRight = director.righthazard.ClosestPointOnBounds(transform.position) - transform.position;
        Vector3 closestTop = director.tophazard.ClosestPointOnBounds(transform.position) - transform.position;
        Vector3 closestBot = director.bothazard.ClosestPointOnBounds(transform.position) - transform.position;
        distanceToHazard = closestLeft.magnitude;
        distanceToHazard = Mathf.Min(distanceToHazard, closestRight.magnitude);
        distanceToHazard = Mathf.Min(distanceToHazard, closestTop.magnitude);
        distanceToHazard = Mathf.Min(distanceToHazard, closestBot.magnitude);
        nearHazard = distanceToHazard < 2.5f;
        diffToTarget = targetLocation - transform.position;
		distanceToTarget = diffToTarget.magnitude;
        if(obstacle)
        {
            distanceToObstacle = (obstacle.transform.position - transform.position).magnitude;
            nearObstacle = distanceToObstacle < 2.0f;
        }

    }
	
	// Update is called once per frame
	public virtual void Update () {
        Measure();
        Move();
    }

    public void Move() {
       
        if(obstacle)
        {
            Vector3 looktarget = obstacle.transform.position;
            looktarget.y = transform.position.y;
            transform.LookAt(looktarget);
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            if (distanceToTarget > 1.0f)
            {
                GetComponent<Rigidbody>().AddForce(diffToTarget*10.0f, ForceMode.Acceleration);
            }
            else if (distanceToTarget > 0.4f)
            {
                GetComponent<Rigidbody>().AddForce(diffToTarget*2.0f, ForceMode.Acceleration);
            }
        }
	}

    public void Retarget(List<Obstacle> ObstacleList)
    {
        int v = Random.Range(0,ObstacleList.Count);
        obstacle = ObstacleList[v];
        transform.LookAt(obstacle.transform.position);
        targetLocation = transform.position;
    }

    public void SetTarget(Vector3 InTargetLocation)
    {
        targetLocation = InTargetLocation;
    }
}
