﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aibot : Mainbot {

    
	private float rethink;
	
	// Update is called once per frame
	public override void Update () {
        Measure();
        if(!obstacle || obstacle.transform.position.y < -1.0f)
        {
            Retarget(director.o);
        }
        else
        {
            rethink -= Time.deltaTime;
            if(rethink < 0.0f)
            {
                if(nearHazard)
                {
                    Retarget(director.o);
                }
                SetTarget(transform.position + transform.forward * 2.0f);
                rethink = 1.0f;
            }
            
        }
		Move();
	}
}
