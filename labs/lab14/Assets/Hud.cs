﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hud : MonoBehaviour {

	public Director director;
	public Text lastValue;
	public Text curValue;
	public Text nearHaz;
	public Text nearObs;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		lastValue.text = director.LastReward.ToString();
		curValue.text = director.Reward.ToString();
		if(director.p)
		{
			nearHaz.gameObject.SetActive(director.p.nearHazard);
			nearObs.gameObject.SetActive(director.p.nearObstacle);
		}
	}
}
