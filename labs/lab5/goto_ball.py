#!/usr/bin/env python3

import asyncio
import sys

import cv2
import numpy as np

import find_ball

import cozmo
import time

try:
    from PIL import ImageDraw, ImageFont
except ImportError:
    sys.exit('run `pip3 install --user Pillow numpy` to run this example')


# Define a decorator as a subclass of Annotator; displays battery voltage
class BatteryAnnotator(cozmo.annotate.Annotator):
    def apply(self, image, scale):
        d = ImageDraw.Draw(image)
        bounds = (0, 0, image.width, image.height)
        batt = self.world.robot.battery_voltage
        text = cozmo.annotate.ImageText('BATT %.1fv' % batt, color='green')
        text.render(d, bounds)

# Define a decorator as a subclass of Annotator; displays the ball
class BallAnnotator(cozmo.annotate.Annotator):

    ball = None

    def apply(self, image, scale):
        d = ImageDraw.Draw(image)
        bounds = (0, 0, image.width, image.height)

        if BallAnnotator.ball is not None:

            #double size of bounding box to match size of rendered image
            BallAnnotator.ball = np.multiply(BallAnnotator.ball,2)

            #define and display bounding box with params:
            #msg.img_topLeft_x, msg.img_topLeft_y, msg.img_width, msg.img_height
            box = cozmo.util.ImageBox(BallAnnotator.ball[0]-BallAnnotator.ball[2]*0.5,
                                      BallAnnotator.ball[1]-BallAnnotator.ball[2]*0.5,
                                      BallAnnotator.ball[2]*2, BallAnnotator.ball[2]*2)
            cozmo.annotate.add_img_box_to_image(image, box, "green", text=None)

            BallAnnotator.ball = None


async def run(robot: cozmo.robot.Robot):
    '''The run method runs once the Cozmo SDK is connected.'''

    #add annotators for battery level and ball bounding box
    robot.world.image_annotator.add_annotator('battery', BatteryAnnotator)
    robot.world.image_annotator.add_annotator('ball', BallAnnotator)

    robot.move_lift(-3)
    robot.set_head_angle(cozmo.robot.MIN_HEAD_ANGLE*0.9+cozmo.robot.MAX_HEAD_ANGLE*0.1).wait_for_completed()

    try:

        timer = 0
        bestball = None
        while True:
            #get camera image
            event = await robot.world.wait_for(cozmo.camera.EvtNewRawCameraImage, timeout=30)

            #convert camera image to opencv format
            opencv_image = cv2.cvtColor(np.asarray(event.image), cv2.COLOR_RGB2GRAY)

            #find the ball
            ball = find_ball.find_ball(opencv_image)

            #set annotator ball
            BallAnnotator.ball = ball

            if ball[2] > 1:
                bestball = ball
            if timer > 3:
                robot.move_lift(3)
            if bestball is not None:
                cv2.imwrite("01.jpg",opencv_image)
                thirdwidth = opencv_image.shape[1]*0.33
                if bestball[0] < thirdwidth:
                    print("mleft",bestball[0])
                    motor_left = 3
                    motor_right = 10
                elif bestball[0] < 2*thirdwidth:
                    speed = 15
                    if bestball[2] > 70:
                        timer += 1
                        speed = 1
                    elif bestball[2] > 40:
                        speed = (200 - bestball[2])/10
                    motor_left = speed
                    motor_right = speed
                    
                    print("go",bestball[2]," speed",speed)
                else:
                    print("mright",bestball[0])
                    motor_left = 10
                    motor_right = 3
            else:
                print("nothin")
                cv2.imwrite("02.jpg",opencv_image)
                motor_left = 20
                motor_right = -5
            # Send commands
            await robot.drive_wheels(motor_left, motor_right)
            
            time.sleep(.1)

            


    except KeyboardInterrupt:
        print("")
        print("Exit requested by user")
    except cozmo.RobotBusy as e:
        print(e)



if __name__ == '__main__':
    cozmo.run_program(run, use_viewer = True, force_viewer_on_top = True)
