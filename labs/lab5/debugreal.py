#!/usr/bin/env python3
 
import cv2
import os
import math
import find_ball
import sys
import numpy as np

filename = ""
if len(sys.argv) > 1:
	filename = sys.argv[-1];
else:
	filename = "01.jpg"

#read in image as grayscale
opencv_image = cv2.imread(filename, cv2.COLOR_GRAY2RGB)

#try to find the ball in the image
ball = find_ball.find_ball(opencv_image, True)