#!/usr/bin/env python3

import cv2
import sys
import copy

import numpy as np

try:
	from PIL import Image, ImageDraw, ImageFont
except ImportError:
	sys.exit('install Pillow to run this code')


def find_ball(opencv_image, debug=False):
	"""Find the ball in an image.
		
		Arguments:
		opencv_image -- the image
		debug -- an optional argument which can be used to control whether
				debugging information is displayed.
		
		Returns [x, y, radius] of the ball, and [0,0,0] or None if no ball is found.
	"""

	ball = None

	median = cv2.medianBlur(opencv_image,3)
	gaus = cv2.GaussianBlur(median,(5,5),5)
	dilate = cv2.dilate(gaus,None,iterations = 3)
	erode = cv2.erode(dilate,None,iterations = 3)

	final = erode


	ball = np.array([0, 0, 0])
	circles = cv2.HoughCircles(final,cv2.HOUGH_GRADIENT,1,7,param1=100,param2=25,minRadius=3,maxRadius=250)
	if circles is None:
		return ball;

	otherarray = []
	for circlegroup in circles:
		for i in circlegroup:
			
			if i[2] > ball[2]:
				ball[0] = i[0]
				ball[1] = i[1]
				ball[2] = i[2]
			else:
				other = np.array([int(i[0]), int(i[1]), int(i[2])])
				otherarray.append(other)

	if debug:
		display_circles(final, otherarray, ball);

	return ball


def display_circles(opencv_image, circles, best=None):
	#make a copy of the image to draw on
	circle_image = copy.deepcopy(opencv_image)
	circle_image = cv2.cvtColor(circle_image, cv2.COLOR_GRAY2RGB, circle_image)

	#highlight the best circle in a different color
	if best is not None:
		# draw the outer circle
		cv2.circle(circle_image,(best[0],best[1]),best[2],(0,0,255),2)
		# draw the center of the circle
		cv2.circle(circle_image,(best[0],best[1]),2,(255,255,255),3) 
		# write coords
		cv2.putText(circle_image,str(best),(best[0],best[1]),cv2.FONT_HERSHEY_SIMPLEX,
					.3,(255,255,255),1,cv2.LINE_AA)            
		
	
	#display the image
	pil_image = Image.fromarray(circle_image)
	pil_image.show()    
	  
if __name__ == "__main__":
	pass
