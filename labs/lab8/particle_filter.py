from grid import *
from particle import Particle
from utils import *
from setting import *
from math import pi, cos, sin, exp, sqrt, radians
import numpy
import bisect
import random

def motion_update(particles, odom):
    """ Particle filter motion update

        Arguments: 
        particles -- input list of particle represents belief p(x_{t-1} | u_{t-1})
                before motion update
        odom -- odometry to move (dx, dy, dh) in *robot local frame*

        Returns: the list of particles represents belief \tilde{p}(x_{t} | u_{t})
                after motion update
    """
    motion_particles = []
    odom_x = odom[0]
    odom_y = odom[1]
    odom_h = odom[2]
    for p in particles:
        next_degree = p.h + odom_h
        if next_degree > 360:
            next_degree -= 360
        if next_degree < 0:
            next_degree += 360
        dx, dy = rotate_point(odom_x, odom_y, next_degree)
        #adjust the particle by the delta x, y and h, where the h is a sanitized degree
        mp = Particle(p.x + dx, p.y + dy, next_degree)
        motion_particles.append(mp)
    return motion_particles


def gaussian_h(ah, bh):
    ah = 5
    bh = 25
    # scale the difference by 0.1, so 10degrees is error of 1
    dh = diff_heading_deg(ah, bh)*0.1
    error_sq = numpy.power(dh, 2.)
    #selected a sigma of 0.99
    sigma_doub_sq = 2 * numpy.power(0.98, 2.)
    return numpy.exp(-error_sq / sigma_doub_sq)

def guassian_xyh(ax, ay, ah, bx, by, bh):
    #mixture of an distance gaussian and a heading gaussian
    return 0.85*guassian_xy(ax, ay, bx, by) + 0.15*gaussian_h(ah, bh)


def guassian_xy(ax, ay, bx, by):
    #scale the difference by 0.8
    dx = (ax - bx)*0.8
    dy = (ay - by)*0.8
    dist_sq = (dx * dx + dy * dy)
    # selected a sigma of 0.99
    sigma_doub_sqr = 2 * numpy.power(0.98, 2.)
    return numpy.exp(-dist_sq / sigma_doub_sqr)

def weight_from_error(particle, grid, detected_markers):
    best_weight = 0

    for gm in grid.markers:
        real_x, real_y, real_h = parse_marker_info(gm[0], gm[1], gm[2])
        for dm in detected_markers:
            dx, dy = rotate_point(dm[0], dm[1], particle.h)
            estimate_x = dx + particle.x
            estimate_y = dy + particle.y
            estimate_h = dm[2] + particle.h
            while estimate_h > 359:
                estimate_h -= 360
            while estimate_h < -1:
                estimate_h += 360
            weight = guassian_xyh(estimate_x, estimate_y, estimate_h, real_x, real_y, real_h)
            if weight > best_weight:
                best_weight = weight



    if grid.is_in(particle.x, particle.y):
        return best_weight
    else:
        return best_weight*0.66
# ------------------------------------------------------------------------
def measurement_update(particles, measured_marker_list, grid):
    """ Particle filter measurement update

        Arguments: 
        particles -- input list of particle represents belief \tilde{p}(x_{t} | u_{t})
                before meansurement update (but after motion update)

        measured_marker_list -- robot detected marker list, each marker has format:
                measured_marker_list[i] = (rx, ry, rh)
                rx -- marker's relative X coordinate in robot's frame
                ry -- marker's relative Y coordinate in robot's frame
                rh -- marker's relative heading in robot's frame, in degree

                * Note that the robot can only see markers which is in its camera field of view,
                which is defined by ROBOT_CAMERA_FOV_DEG in setting.py
				* Note that the robot can see mutliple markers at once, and may not see any one

        grid -- grid world map, which contains the marker information, 
                see grid.py and CozGrid for definition
                Can be used to evaluate particles

        Returns: the list of particles represents belief p(x_{t} | u_{t})
                after measurement update
    """
    weights = []
    number_of_particles = len(particles)
    weight_sum = 0
    bad_weight = True
    if len(measured_marker_list) > 0:
        for i in range(number_of_particles):
            weight = weight_from_error(particles[i], grid, measured_marker_list)
            weights.append(weight)
            weight_sum += weight
            if i < 5 and weight < 0.8:
                weight = weight_from_error(particles[i], grid, measured_marker_list)
        weight_avg = weight_sum / number_of_particles
        #when weighted average is a bad data, then something went wrong, dont use computed weights
        if len(measured_marker_list) > 0 and weight_avg > 0.001:
            # normalize weights
            weights = [w / weight_sum for w in weights]
            bad_weight = False


    # make a distribution based on probabilities,
    # [0.01, 0.51, 0.8, 0.9] will select the first one 1% of the time and the second one 50%
    weight_accumulate = 0
    weight_distribution = []
    for w in weights:
        weight_accumulate += w
        weight_distribution.append(weight_accumulate)

    next_particles = []
    for src_idx in range(number_of_particles):
        next_x = 0
        next_y = 0
        next_h = None
        # keep 98% and make free place 2% of the time
        if random.random() < 0.98:
            idx = src_idx
            # if the weights are bad, just copy the original particle entry at this index
            if bad_weight is False:
                next_idx = bisect.bisect_left(weight_distribution, random.random())
                if next_idx < number_of_particles:
                    idx = next_idx
            p = particles[idx]
            # add some noise to the selection
            next_x = add_gaussian_noise(p.x, 0.06)
            next_y = add_gaussian_noise(p.y, 0.06)
            next_h = add_gaussian_noise(p.h, 2)
            while next_h > 359:
                next_h -= 360
            while next_h <= -1:
                next_h += 360
        else:
            next_x, next_y = grid.random_free_place()
        next_particles.append(Particle(next_x, next_y, next_h))
    # random choice might be faster
    #next_particles = numpy.random.choice(a=particles, size=number_of_particles, replace=True, p=norm_weights)
    return next_particles


