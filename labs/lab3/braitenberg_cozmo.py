#!/usr/bin/env python3

'''Make Cozmo behave like a Braitenberg machine with virtual light sensors and wheels as actuators.

The following is the starter code for lab.
'''

import asyncio
import time
import cozmo
import cv2
import numpy as np
import sys


def sense_brightness(image, columns):
	
	h = image.shape[0]
	w = image.shape[1]
	avg_brightness = 0

	for y in range(0, h):
		for x in columns:
			avg_brightness += image[y,x]

	avg_brightness /= (h*columns.shape[0])

	return avg_brightness

def max_brightness(image):
	h = image.shape[0]
	w = image.shape[1]
	max_brightness = 0.1

	for y in range(0, h):
		for x in range(0, w):
			if image[y,x] > max_brightness:
				max_brightness = image[y,x]


	return max_brightness

def motor_mapping(sensor_value, factor):
	'''Maps a sensor reading to a wheel motor command'''
	motor_value = factor*sensor_value
	return motor_value

def normalize_sense(val, max):
	if val > max:
		val = 1.0
	elif val < -max:
		val = -1.0
	else:
		val /= max
	return val

async def braitenberg_machine(robot: cozmo.robot.Robot):
	'''The core of the braitenberg machine program'''
	# Move lift down and tilt the head up
	robot.move_lift(-3)
	robot.set_head_angle(cozmo.robot.MAX_HEAD_ANGLE).wait_for_completed()
	print("Press CTRL-C to quit")

	while True:
		
		#get camera image
		event = await robot.world.wait_for(cozmo.camera.EvtNewRawCameraImage, timeout=30)

		#convert camera image to opencv format
		opencv_image = cv2.cvtColor(np.asarray(event.image), cv2.COLOR_RGB2GRAY)
		# Determine the w/h of the new image
		h = opencv_image.shape[0]
		w = opencv_image.shape[1]

		
		# Map the sensors to actuators
		num = 3
		coward = num == 0
		aggressive = num == 1
		love = num == 2
		explorer = num == 3

		sensor_n_columns = int(w/3);
		
		#if love or explorer:
	#		sensor_n_columns = int(w/2);

		sensor_max = max_brightness(opencv_image)
		sensor_ambient = sense_brightness(opencv_image, columns=np.arange(w))/sensor_max
		# Sense the current brightness values on the right and left of the image.
		sensor_right = sense_brightness(opencv_image, columns=np.arange(sensor_n_columns))/sensor_max
		sensor_left = sense_brightness(opencv_image, columns=np.arange(w-sensor_n_columns, w))/sensor_max


		sensor_ambient =normalize_sense(sensor_ambient, 0.12)
		sensor_left =normalize_sense(sensor_left, 0.2)
		sensor_right =normalize_sense(sensor_right, 0.2)

		print("sensor_ambient: " + str(sensor_ambient))
		print("sensor_right: " + str(sensor_right))
		print("sensor_left: " + str(sensor_left))

		fullspeed = 20;
		if coward:
			motor_left = sensor_left * fullspeed
			motor_right = sensor_right * fullspeed
		elif aggressive:
			motor_right = sensor_left * fullspeed
			motor_left = sensor_right * fullspeed
		elif love:
			fullspeed *= 1-sensor_ambient
			motor_right = 3*sensor_left * fullspeed
			motor_left = 3*sensor_right * fullspeed
		elif explorer:
			fullspeed *= 1-sensor_ambient
			motor_left = 3*sensor_left * fullspeed
			motor_right = 3*sensor_right * fullspeed
		print("motor_right: " + str(motor_right))
		print("motor_left: " + str(motor_left))
		print("fullspeed: " + str(fullspeed))

		# Send commands to the robot
		await robot.drive_wheels(motor_right, motor_left)

		time.sleep(.1)


cozmo.run_program(braitenberg_machine, use_viewer=True, force_viewer_on_top=True)
