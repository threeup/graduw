#!/usr/bin/env python

##############
#### Your name: Sheridan Thirsk
##############

import numpy as np
import re
import time
from sklearn import svm, metrics
from skimage import io, feature, filters
from sklearn.naive_bayes import GaussianNB
from skimage.restoration import denoise_wavelet
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC


class ImageClassifier:
    
    def __init__(self):
        self.classifer = None

    def imread_convert(self, f):
        return io.imread(f).astype(np.uint8)

    def load_data_from_folder(self, dir):
        # read all images into an image collection
        ic = io.ImageCollection(dir+"*.bmp", load_func=self.imread_convert)
        
        #create one large array of image data
        data = io.concatenate_images(ic)
        
        #extract labels from image names
        labels = np.array(ic.files)
        for i, f in enumerate(labels):
            m = re.search("_", f)
            labels[i] = f[len(dir):m.start()]
        
        return(data,labels)

    def rgb2gray(self, rgb):
        r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
        gray = 0.30 * r + 0.50 * g + 0.20 * b
        return gray

    def extract_image_features(self, data):
        # Please do not modify the header above

        # extract feature vector from image data
        feature_data = []
        for img in data:
            blurred_img = denoise_wavelet(img, multichannel=True)
            gray_img = self.rgb2gray(blurred_img)
            blurred_img = filters.scharr(gray_img)
            final_img = blurred_img
            hog_hys = feature.hog(final_img, orientations=8, pixels_per_cell=(22, 22),
                                 cells_per_block=(9, 9), block_norm='L2-Hys',
                                 feature_vector=True, transform_sqrt=False)

            feature_data.append(hog_hys)
        # Please do not modify the return type below
        return(feature_data)

    def train_classifier(self, train_data, train_labels):
        # Please do not modify the header above
        
        # train model and save the trained model to self.classifier

        #cla = MLPClassifier(alpha=1) #0.975 , slow
        cla = SVC(kernel="linear", C=0.025) #0.975
        #cla = KNeighborsClassifier(3) #0.9
        #cla = GaussianNB() #0.95
        self.classifer = cla.fit(train_data, train_labels)
        return

    def predict_labels(self, data):
        # Please do not modify the header

        # predict labels of test data using trained model in self.classifier
        # the code below expects output to be stored in predicted_labels

        predicted_labels = self.classifer.predict(data)
        
        # Please do not modify the return type below
        return predicted_labels

      
def main():
    start_time = time.time()

    img_clf = ImageClassifier()

    # load images
    (train_raw, train_labels) = img_clf.load_data_from_folder('./train/')
    (test_raw, test_labels) = img_clf.load_data_from_folder('./test/')
    
    # convert images into features
    train_data = img_clf.extract_image_features(train_raw)
    test_data = img_clf.extract_image_features(test_raw)
    
    # train model and test on training data
    img_clf.train_classifier(train_data, train_labels)
    predicted_labels = img_clf.predict_labels(train_data)
    print("\nTraining results")
    print("=============================")
    print("Confusion Matrix:\n",metrics.confusion_matrix(train_labels, predicted_labels))
    print("Accuracy: ", metrics.accuracy_score(train_labels, predicted_labels))
    print("F1 score: ", metrics.f1_score(train_labels, predicted_labels, average='micro'))
    
    # test model
    predicted_labels = img_clf.predict_labels(test_data)
    print("\nTraining results")
    print("=============================")
    print("Confusion Matrix:\n",metrics.confusion_matrix(test_labels, predicted_labels))
    print("Accuracy: ", metrics.accuracy_score(test_labels, predicted_labels))
    print("F1 score: ", metrics.f1_score(test_labels, predicted_labels, average='micro'))

    elapsed_time = time.time() - start_time
    print("Elapsed", elapsed_time)


if __name__ == "__main__":
    main()
