# Author : Vincent Michel, 2010
#          Alexandre Gramfort, 2011
# License: BSD 3 clause

print(__doc__)

import time as time

import numpy as np
import scipy as sp

import matplotlib.pyplot as plt


from sklearn import svm, metrics
from skimage import io, feature, filters, exposure, color, morphology, data
from sklearn.naive_bayes import GaussianNB
from skimage.restoration import (denoise_tv_chambolle, denoise_bilateral,
                                 denoise_wavelet, estimate_sigma, unsupervised_wiener)
from imgclassification import ImageClassifier
from scipy.signal import convolve2d
from skimage.morphology import disk

def rgb2gray(rgb):
    r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray

# #############################################################################
# Generate data
#image = sp.misc.face()
#image = rgb2gray(image)
img_clf = ImageClassifier()
(train_raw, train_labels) = img_clf.load_data_from_folder('./train/')
img = train_raw[0]
img = rgb2gray(img)
#blurred_img = denoise_bilateral(img, sigma_color=0.05, sigma_spatial=15, multichannel=True)
blurred_img = denoise_wavelet(img, multichannel=False)


middle_img = filters.frangi(img)

final_img = filters.sobel(middle_img)

# #############################################################################
# Plot the results on an image
plt.figure(figsize=(5, 5))
plt.imshow(img, cmap=plt.cm.gray)
plt.xticks(())
plt.yticks(())
plt.figure(figsize=(5, 5))
plt.imshow(middle_img, cmap=plt.cm.gray)
plt.xticks(())
plt.yticks(())
plt.figure(figsize=(5, 5))
plt.imshow(final_img, cmap=plt.cm.gray)
plt.xticks(())
plt.yticks(())
plt.show()