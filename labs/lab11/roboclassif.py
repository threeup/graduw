#!/usr/bin/env python

##############
#### Your name: Sheridan Thirsk
##############

import numpy as np
import re
import time
import sys

import cv2
import numpy as np


import cozmo
import time
from PIL import ImageDraw, ImageFont, Image

from sklearn import svm, metrics
from skimage import io, feature, filters
from sklearn.naive_bayes import GaussianNB
from skimage.restoration import denoise_wavelet
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC


class ImageClassifier:

    def __init__(self):
        self.classifer = None

    def imread_convert(self, f):
        return io.imread(f).astype(np.uint8)

    def load_data_from_folder(self, dir):
        # read all images into an image collection
        ic = io.ImageCollection(dir+"*.jpg", load_func=self.imread_convert)

        #create one large array of image data
        data = io.concatenate_images(ic)

        #extract labels from image names
        labels = np.array(ic.files)
        for i, f in enumerate(labels):
            m = re.search("_", f)
            labels[i] = f[len(dir):m.start()]

        return(data,labels)

    def rgb2gray(self, rgb):
        r, g, b = rgb[:, :, 0], rgb[:, :, 1], rgb[:, :, 2]
        gray = 0.30 * r + 0.50 * g + 0.20 * b
        return gray

    def extract_image_features(self, data):
        # Please do not modify the header above

        # extract feature vector from image data
        feature_data = []
        for img in data:
            blurred_img = denoise_wavelet(img, multichannel=True)
            gray_img = self.rgb2gray(blurred_img)
            blurred_img = filters.scharr(gray_img)
            final_img = blurred_img
            hog_hys = feature.hog(final_img, orientations=8, pixels_per_cell=(22, 22),
                                 cells_per_block=(9, 9), block_norm='L2-Hys',
                                 feature_vector=True, transform_sqrt=False)

            feature_data.append(hog_hys)
        # Please do not modify the return type below
        return(feature_data)

    def train_classifier(self, train_data, train_labels):
        # Please do not modify the header above

        # train model and save the trained model to self.classifier

        #cla = MLPClassifier(alpha=1) #0.975 , slow
        cla = SVC(kernel="linear", C=0.025) #0.975
        #cla = KNeighborsClassifier(3) #0.9
        #cla = GaussianNB() #0.95
        self.classifer = cla.fit(train_data, train_labels)
        return

    def predict_labels(self, data):
        # Please do not modify the header

        # predict labels of test data using trained model in self.classifier
        # the code below expects output to be stored in predicted_labels

        predicted_labels = self.classifer.predict(data)

        # Please do not modify the return type below
        return predicted_labels




class ThingAnnotator(cozmo.annotate.Annotator):
    identified = None

    def apply(self, image, scale):
        d = ImageDraw.Draw(image)
        bounds = (0, 0, image.width, image.height)

        if ThingAnnotator.identified is not None:

            # define and display bounding box with params:
            # msg.img_topLeft_x, msg.img_topLeft_y, msg.img_width, msg.img_height
            box = cozmo.util.ImageBox(ThingAnnotator.identified[0] - ThingAnnotator.identified[2] * 0.5,
                                      ThingAnnotator.identified[1] - ThingAnnotator.identified[2] * 0.5,
                                      ThingAnnotator.identified[2] * 2, ThingAnnotator.identified[2] * 2)
            cozmo.annotate.add_img_box_to_image(image, box, "green", text=None)

            ThingAnnotator.identified = None


async def run(robot: cozmo.robot.Robot):
    '''The run method runs once the Cozmo SDK is connected.'''

    robot.world.image_annotator.add_annotator('thing', ThingAnnotator)


    img_clf = ImageClassifier()

    # load images
    (train_raw, train_labels) = img_clf.load_data_from_folder('./robotrain/')

    # convert images into features
    train_data = img_clf.extract_image_features(train_raw)
    print("extracted")

    # train model and test on training data
    img_clf.train_classifier(train_data, train_labels)

    print("learned")
    try:
        robot.move_lift(-3)
        await robot.set_head_angle(cozmo.robot.MIN_HEAD_ANGLE * 0.5 + cozmo.robot.MAX_HEAD_ANGLE * 0.5).wait_for_completed()
        time.sleep(.1)

        good_one = None
        good_two = None
        good_three = None
        good_four = None
        good_five = None
        good_six = None
        num = 0
        label = None
        while True:
            # get camera image
            event = await robot.world.wait_for(cozmo.camera.EvtNewRawCameraImage, timeout=30)
            robot_images = [event.image]
            robot_features = img_clf.extract_image_features(robot_images)
            predicted_labels = img_clf.predict_labels(robot_features)
            label = predicted_labels[0];
            if label is not None and label != 'none':

                if label == good_one and label == good_two and label == good_three and label == good_four \
                        and label == good_five and label == good_six:
                    event.image.save("good"+label+str(num)+".jpg")

                    num += 1
                    good_one = "none"
                    good_two = "none"
                    good_three = "none"
                    good_four = "none"
                    good_five = "none"
                    good_six = "none"

                    print(label)
                    if label == "plane":
                        await robot.say_text("plane").wait_for_completed()
                        robot.move_lift(9)
                    if label == "hands":
                        await robot.say_text("hands").wait_for_completed()
                        robot.move_lift(3)
                    if label == "order":
                        await robot.say_text("order").wait_for_completed()
                        robot.move_lift(5)
                    if label == "truck":
                        await robot.say_text("truck").wait_for_completed()
                        robot.move_lift(0)
                    if label == "inspection":
                        await robot.say_text("inspection").wait_for_completed()
                        robot.move_lift(-3)
                    if label == "drone":
                        await robot.say_text("drone").wait_for_completed()
                        robot.move_lift(-3)

                good_six = good_five
                good_five = good_four
                good_four = good_three
                good_three = good_two
                good_two = good_one
                good_one = label

            time.sleep(.1)

    except KeyboardInterrupt:
        print("")
    except cozmo.RobotBusy as e:
        print(e)


if __name__ == '__main__':
    cozmo.run_program(run, use_viewer=True, force_viewer_on_top=True)
